function Sound(source)
{
    this.source = source;
    var dom;
    this.dom = dom;
    this.isplaying = false;
    this.stop=function()
    {   
        if (this.isplaying) {
            this.isplaying = false;
            document.body.removeChild(this.dom);
        }
    }
    this.start=function()
    {   
        this.isplaying = true;
        this.dom=document.createElement("audio");
        this.dom.setAttribute("src",this.source);
        this.dom.setAttribute("autoplay","true");
        document.body.appendChild(this.dom);
    }
}