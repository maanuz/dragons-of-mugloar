'use strict';

var app = angular.module('Dragon', ['ui.router', 'ngSanitize'] );
var api = "http://www.dragonsofmugloar.com/";
app
.run(['$rootScope', '$location', '$window', function($rootScope, $location, $window) {

}])
.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('game', {
                url: '/',
                templateUrl: 'views/game.html',
                controller: 'GameCtrl'
            })
    }
])