app

/*
 * At page load, initialize default data and stats
 */
.controller('MasterCtrl', ['$scope', function ($scope) {
	$scope.stats = {
		victorys: 0,
		defeats: 0
	}
	$scope.gameStatus = 'start';
	$scope.gameData = {};

	$scope.loading = {
		status: false
	}
	$scope.dragonRoar = new Sound("sounds/dragon.mp3");
}])

/*
 * Controller used inside game
 */
.controller('GameCtrl', ['$scope', 'Data', function ($scope, Data) {

	/*
	 * Search for a new knight to attack, or just a function hiding behind new game button
	 */
	$scope.startNewGame = function() {
		$scope.loading.status = true;
		Data.get("api/game/").success(function(response) {
			$scope.gameData = response;
			$scope.gameStatus = 'knight';
			$scope.getWeather();
		})
	}
	$scope.getWeather = function() {
		Data.get("weather/api/report/" + $scope.gameData.gameId).success(function(response) {
			var xmlDoc = $.parseXML(response);
			$scope.weatherCode = $(xmlDoc).find('code').text();
			$scope.weatherStatus = $(xmlDoc).find('message').text();
			$scope.loading.status = false;
		})
	}

	/*
	 * Funtion to set stats acording to weather report
	 */
	$scope.setStats = function(code) {
		//Fire is useless in heavy rain, need sharper claws to destroy the umbrellaboats
		if (code == 'HVA') {
			return {
		        "scaleThickness": 5,
		        "clawSharpness": 10,
		        "wingStrength": 5,
		        "fireBreath": 0
		    }
		} else {
			return {
		        "scaleThickness": $scope.gameData.knight.attack,
		        "clawSharpness": $scope.gameData.knight.armor,
		        "wingStrength": $scope.gameData.knight.agility,
		        "fireBreath": $scope.gameData.knight.endurance
		    }
		}
	}

	/*
	 * Funtion to find the biggest stat value in object
	 */
	$scope.getBiggestIndex = function(object) {
		$scope.biggestStatIndex = null;
		for (i in object) {
			if ($scope.biggestStatIndex == null || object[i] > object[$scope.biggestStatIndex] ) {
				$scope.biggestStatIndex = i;
			}
		}
		return $scope.biggestStatIndex;
	}

	/*
	 * Funtion to find the smallest stat value in object
	 */
	$scope.getSmallestIndex = function(object) {
		$scope.smallestStatIndex = null;
		for (i in object) {
			if ($scope.smallestStatIndex == null || object[i] < object[$scope.smallestStatIndex] && i != $scope.biggestStatIndex ) {
				$scope.smallestStatIndex = i;
			}
		}
		return $scope.smallestStatIndex;
	}

	/*
	 * To battle and beyond!
	 */
	$scope.attackKnight = function() {
		$scope.dragonRoar.start();
		$scope.loading.status = true;
		var dragon = {
		    "dragon": $scope.setStats($scope.weatherCode)
		}
		//When its stormy, ignore battle, we dont want to die, do we?
		if ($scope.weatherCode == 'SRO') {
			$scope.gameStatus = 'end';
			$scope.battleStats = {status: 'Stormy knight', message: 'Dragons dont like storm, so better stay safe.'}
			return;
		}
		//When its normal weather, align stats before attacking
		if ($scope.weatherCode == 'NMR') {
			$scope.getBiggestIndex(dragon.dragon);
			$scope.getSmallestIndex(dragon.dragon);
			for (i in dragon.dragon) {
				if (i == $scope.biggestStatIndex) {
					dragon.dragon[i] += 2;
				} else if (i != $scope.smallestStatIndex) {
					dragon.dragon[i]--;
				}
			}
		}
		Data.put('api/game/' + $scope.gameData.gameId + "/solution", dragon).success(function(response) {
			$scope.gameStatus = 'end';
			$scope.battleStats = response;
			if (response.status == 'Victory') {
				$scope.stats.victorys++;
			} else {
				$scope.stats.defeats++;
			}
			$scope.loading.status = false;
		});
	}
}])